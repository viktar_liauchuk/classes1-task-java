# Code Exercise

Create the package `task` and define the class `BusinessTrip` inside it 
to represent an employee's trip.

### Class fields:
* daily allowance rate in Euro (the constant),
* employee`s account,
* transportation expenses in Euro,
* number of days.

### Constructors:
* no-arg constructor;
* parameterized constructor.

### Methods:
* getters/setters;
* `getTotal` – returns the total business trip expenses in Euro
<br/>(= transportation expenses + daily allowance rate * number of days);
* `show` – outputs all fields to the console (each field and the total business trip expenses should be in separate lines in the following format: `name=value`);
  <br/>Example:
  <br/>`rate = 23.00`
  <br/>`account = Anton Shumsky`
  <br/>`transport = 36.20`
  <br/>`days = 5`
  <br/>`total = 151.20`
* `toString` – returns a string representation of a business trip in the csv–format: each non constant field and the total business trip expenses, separated by the ';' symbol.
  <br/>Example:
  <br/>`Anton Shumsky;36.20;5;151.20`

Define the `Runner` class in the default package, where:
1. Create an array of minimum 5 objects (the element with the index 2 should be empty; the last element of the array should be created by the no-arg constructor; other elements are valid and should be created by the parameterized constructor).
2. Output the array content to the console, using `show` method, and the business trip with maximum cost.
3. Update the employee`s transportation expenses for the last object of the array.
4. Output the total duration of two initial business trips by the single operator.
   <br/>Example:
   <br/>`Duration = 9`
5. Output the array content to the console (one element per line), using `toString` method implicitly.
